package com.example.mypantry

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fridgeButton.setOnClickListener{
            startActivity(Intent(this, FridgeActivity::class.java))
        }
        freezerButton.setOnClickListener{
            startActivity(Intent(this, FrozenActivity::class.java))
        }
        pantryButton.setOnClickListener{
            startActivity(Intent(this, PantryActivity::class.java))
        }
    }

}


