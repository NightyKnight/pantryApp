package com.example.mypantry

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.pantry_layout.view.*

class PantryActivity : AppCompatActivity() {

    var database: FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pantry)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val PantryList = arrayListOf<Pantry?>()
        recyclerView.layoutManager = LinearLayoutManager(this)

        database = FirebaseDatabase.getInstance()


        class PantryAdapter(val items: ArrayList<Pantry?>) : RecyclerView.Adapter<PantryAdapter.PantryViewHolder>() {
            inner class PantryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
                var itemName = itemView.itemName
                var qty = itemView.itemQuantity
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PantryViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.pantry_layout, parent, false)
                return PantryViewHolder(view)
            }

            override fun getItemCount(): Int {
                return items.size
            }

            override fun onBindViewHolder(holder: PantryViewHolder, position: Int) {
                holder.itemName.text = items[position]!!.name
                holder.qty.text = "Quantity: ${items[position]!!.quantity.toString()}"

            }
        }


        database!!.reference
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.e("PantryActivity", "${p0.toException()}")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    PantryList.clear()
                    for (snapshot in p0.child("Pantry").children) {
                        val pantry = snapshot.getValue(Pantry::class.java)
                        PantryList.add(pantry)
                    }

                    val myAdapter = PantryAdapter(PantryList)
                    recyclerView.adapter = myAdapter
                }

            })


    }
}
