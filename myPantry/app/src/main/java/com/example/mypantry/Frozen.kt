package com.example.mypantry

class Frozen (val name: String, val quantity: Int){
    constructor() : this("", 1)
}