package com.example.mypantry

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.frozen_layout.view.*

class FrozenActivity : AppCompatActivity() {

    var database: FirebaseDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frozen)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val frozenList = arrayListOf<Frozen?>()
        recyclerView.layoutManager = LinearLayoutManager(this)

        database = FirebaseDatabase.getInstance()


        class FrozenAdapter(val items: ArrayList<Frozen?>) : RecyclerView.Adapter<FrozenAdapter.FrozenViewHolder>() {
            inner class FrozenViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
                var itemName = itemView.itemName
                var qty = itemView.itemQuantity
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FrozenViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.frozen_layout, parent, false)
                return FrozenViewHolder(view)
            }

            override fun getItemCount(): Int {
                return items.size
            }

            override fun onBindViewHolder(holder: FrozenViewHolder, position: Int) {
                holder.itemName.text = items[position]!!.name
                holder.qty.text = "Quantity: ${items[position]!!.quantity.toString()}"

            }
        }


        database!!.reference
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.e("FrozenActivity", "${p0.toException()}")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    frozenList.clear()
                    for (snapshot in p0.child("Freezer").children) {
                        val frozen = snapshot.getValue(Frozen::class.java)
                        frozenList.add(frozen)
                    }

                    val myAdapter = FrozenAdapter(frozenList)
                    recyclerView.adapter = myAdapter
                }

            })


    }
}

