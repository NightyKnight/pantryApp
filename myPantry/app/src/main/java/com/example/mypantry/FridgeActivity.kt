package com.example.mypantry

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fridge_layout.view.*

class FridgeActivity : AppCompatActivity() {

    var database: FirebaseDatabase? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fridge)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val fridgeList = arrayListOf<Items?>()
        recyclerView.layoutManager = LinearLayoutManager(this)

        database = FirebaseDatabase.getInstance()


        class FridgeAdapter(val items: ArrayList<Items?>) : RecyclerView.Adapter<FridgeAdapter.FridgeViewHolder>() {
            inner class FridgeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
                var itemName = itemView.itemName
                var itemExp = itemView.expiryDate
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FridgeViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.fridge_layout, parent, false)
                return FridgeViewHolder(view)
            }

            override fun getItemCount(): Int {
                return items.size
            }

            override fun onBindViewHolder(holder: FridgeViewHolder, position: Int) {
                holder.itemName.text = items[position]!!.name
                holder.itemExp.text = "Good For: ${items[position]!!.expiry}"

            }
        }


        database!!.reference
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError) {
                    Log.e("FridgeActivity", "${p0.toException()}")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    fridgeList.clear()
                    for (snapshot in p0.child("Items").children) {
                        val fridge = snapshot.getValue(Items::class.java)
                        fridgeList.add(fridge)
                    }

                    val myAdapter = FridgeAdapter(fridgeList)
                    recyclerView.adapter = myAdapter
                }

            })


    }
}
