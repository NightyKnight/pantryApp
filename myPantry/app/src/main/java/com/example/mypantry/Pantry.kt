package com.example.mypantry

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class Pantry (val name: String, val quantity: Int){
    constructor() : this("", 1)
}
